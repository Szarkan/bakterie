#pragma once

#include "Vector2.h"
#include <cstdlib>
#include <thread>
#include <cmath>

class Arena
{
	Vector2 size;
	bool** food;
	
	thread spawner;
thread getch3v2;
public:
	Arena();
	~Arena();
	Arena(Vector2 size,pthread_mutex_t* tmtx_pause, pthread_cond_t* tcond_pause,pthread_mutex_t* tmtx_start, pthread_cond_t* tcond_start);

	Vector2 findFood(Vector2 position);
	bool eat(Vector2 position);
	Vector2 getSize();

	void getch3();
	void print();
	void spawnFood();

		pthread_mutex_t* mtx_pause; 
	pthread_cond_t* cond_pause;

	pthread_mutex_t* mtx_start; 
	pthread_cond_t* cond_start;

};


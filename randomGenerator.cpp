
#include "randomGenerator.h"
#include <cstdlib>
#include <time.h>
#include <iostream>

using namespace std;

randomGenerator::randomGenerator()
{
	srand(time(NULL));
	T = std::thread(&randomGenerator::setValue, this);
	value = 0;
}


randomGenerator::~randomGenerator()
{
}

int randomGenerator::getValue()
{
	return value;
}

int randomGenerator::getValue2()
{
	return value2;
}

void randomGenerator::setValue()
{
	while (true)
	{
		value = std::rand();
		value2 = std::rand();
		//std::cout << value << " " << value2 << endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

	}
	
}

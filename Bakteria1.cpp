﻿
#include "Bakteria1.h"
#include <thread>
#include <ncurses.h>

Bakteria1::Bakteria1()
{
	thread_main = std::thread(&Bakteria1::run, this);
}


Bakteria1::~Bakteria1()
{
}

Bakteria1::Bakteria1(list<Bakteria1*>* lista,  Arena * arena, randomGenerator * R, short type,pthread_mutex_t* tmtx_pause, pthread_cond_t* tcond_pause,pthread_mutex_t* tmtx_start, pthread_cond_t* tcond_start)
{
	bactery_list = lista;
	this->type = type;
	this->arena = arena;
	RandomGenerator = R;
	this->arenaSize = arena->getSize();
	
position.x = rand()%(arenaSize.x);
position.y = rand()%(arenaSize.y);

	this->mtx_start = tmtx_start;
	this->mtx_pause = tmtx_pause;
	this->cond_start = tcond_start;
	this->cond_pause = tcond_pause;

	thread_main = std::thread(&Bakteria1::run, this);
	thread_wait = std::thread(&Bakteria1::wait_for_pause, this);
	


	bactery_list->push_back(this);

}
void Bakteria1::wait_for_pause()
{
	while(!death)
	{
		pthread_cond_wait(cond_start,mtx_start);
		//&mtx_start->unlock();
		pause = true;
	}
}
void Bakteria1::run()
{
	while (!death)
	{
		//cout << pause << endl;
		if (pause)
		{
			cout << "czekam na sygnal";
			pthread_cond_wait(cond_pause, mtx_pause);
			//&mtx_pause->unlock();
			cout << "doczekalem sie";
			pause = false;

		}

		switch (type)
		{
		case 0:
			//cout << "0";
			runType0();
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
			break;
		case 1:
			runType1();
		std::this_thread::sleep_for(std::chrono::milliseconds(600));
			break;
		default:
			break;
		}

		
		
	}

	position.x = arena->getSize().x+1;
	position.y = arena->getSize().y+1;

}

void Bakteria1::runType1()
{
	Bakteria1* nearest_bactery = find_nearest_bactery();
	Vector2 vector_to_move = Vector2();
	if (nearest_bactery != this)
	{
	//cout << "x" << endl;
		Vector2 vector_to_nearest_bactery = Vector2(nearest_bactery->getPosition().x - this->position.x, nearest_bactery->getPosition().y - this->position.y);
		//cout << " Vec: " << vector_to_nearest_bactery.x << " " << vector_to_nearest_bactery.y << endl;
		if (vector_to_nearest_bactery.length() > 1.9)
		{
			
			if (vector_to_nearest_bactery.x > 1)
				vector_to_move.x = 1;
			else if (vector_to_nearest_bactery.x < -1)
				vector_to_move.x = -1;

			if (vector_to_nearest_bactery.y > 1)
				vector_to_move.y = 1;
			else if (vector_to_nearest_bactery.y < -1)
				vector_to_move.y = -1;
		}
		else
		{
		//	cout << "length" << vector_to_nearest_bactery.length() << endl;
			eat(nearest_bactery);
			cout << "EAT";
		}
	}
	else
	{

		//cout << "Rand" << endl;
		//cout << endl;
		//cout << RandomGenerator->getValue() % 3 - 1 << endl;
		//cout << RandomGenerator->getValue2() % 3 - 1 << endl;
		vector_to_move.x = RandomGenerator->getValue() % 3 - 1 ;
		vector_to_move.y = RandomGenerator->getValue2() % 3 - 1 ;
		//cout << vector_to_move.x << " " << vector_to_move.y << "\n";
	}
	//cout << "\n Pause : " << pause;
	move1(vector_to_move);
}

void Bakteria1::runType0()
{
	Vector2 nearest_food = arena->findFood(position);
	Vector2 vector_to_move = Vector2();

	//cout << nearest_food.x << " " << nearest_food.y << endl;
	if ((nearest_food.x != -1) && (nearest_food.y != -1))
	{
		Vector2 vector_to_food;
		vector_to_food.x = nearest_food.x - position.x;
		vector_to_food.y = nearest_food.y - position.y;
cout << nearest_food.length() << endl;
		if (vector_to_food.length() > 1.9)
		{
			if (vector_to_food.x > 1)
				vector_to_move.x = 1;
			else if (vector_to_food.x < -1)
				vector_to_move.x = -1;

			if (vector_to_food.y > 1)
				vector_to_move.y = 1;
			else if (vector_to_food.y < -1)
				vector_to_move.y = -1;
		}
		else
		{
			cout <<" prepare eat " << endl;
			eat(nearest_food);
		}
	}
	else
	{

		vector_to_move.x = RandomGenerator->getValue() % 3 - 1 ;
		vector_to_move.y = RandomGenerator->getValue2() % 3 - 1 ;
	}

	move1(vector_to_move);
	//cout << vector_to_move.x << " " << vector_to_move.y << endl;
	//cout<< nearest_food.x << " " << nearest_food.y << endl << endl;
	
}

Bakteria1* Bakteria1::find_nearest_bactery()
{
	float distance = 10000000;
	Bakteria1* bactery = this;

	////for (auto const i : *bactery_list)
	for (list<Bakteria1*>::iterator i = bactery_list->begin(); i != bactery_list->end(); ++i)
	{
		if ((*i)->getType() == 0 && (*i)->getPosition().x != arena->getSize().x+1)
		{
			float curDistance = sqrt(pow(((*i)->getPosition().x - this->getPosition().x),2) + pow(((*i)->getPosition().y - this->getPosition().y),2));
			if (curDistance < distance)
			{
				distance = curDistance;
				bactery = (*i);
			}
		}
	}

	return bactery;
}

void Bakteria1::move1(Vector2 vector)
{
	//cout << position.x << " " << position.y << "\n";
	position.x += vector.x;
	position.y += vector.y;
	
	if (position.x > arenaSize.x)
	{
		position.x = arenaSize.x + (arenaSize.x - position.x);
	}
	else if (position.x < 0)
	{
		position.x = -position.x;
	}

	if (position.y > arenaSize.y)
	{
		position.y = arenaSize.y + (arenaSize.y - position.y);
	}
	else if (position.y < 0)
	{
		position.y = -position.y;
	}
}

void Bakteria1::eat(Bakteria1* food)
{
	food->kill();
	hungry += 5;
	if (hungry > 10)
		hungry = 10;

}

void Bakteria1::eat(Vector2 position)
{
	cout << "eat:" << position.x << " " << position.y << endl;
	if (arena->eat(position))
	{
		hungry += 5;
		if (hungry > 10)
			hungry = 10;
	}
}

short Bakteria1::getType()
{
	return type;
}

Vector2 Bakteria1::getPosition()
{
	return position;
}

void Bakteria1::kill()
{
	death = true;
}



#pragma once

using namespace std;
class Vector2
{

public:

	int x;
	int y;

	Vector2();
	Vector2(int tx, int ty);
	~Vector2();

	float length();
};



#include "Vector2.h"
#include <math.h>
#include <iostream>

using namespace std;

Vector2::Vector2()
{
	x = 0;
	y = 0;
}

Vector2::Vector2(int tx, int ty)
{
	x = tx;
	y = ty;
}


Vector2::~Vector2()
{
}

float Vector2::length()
{	
	//std::cout << "x: " << x << " y: " << y << " ";
	float tmp = sqrt(pow(x,2) + pow(y,2));
	//cout << " L: " <<tmp << endl;
	return tmp;
}

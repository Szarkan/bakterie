
#include "Arena.h"
#include <iostream>
#include <ncurses.h>

Arena::Arena()
{
}


Arena::~Arena()
{
}

Arena::Arena(Vector2 size,pthread_mutex_t* tmtx_pause, pthread_cond_t* tcond_pause,pthread_mutex_t* tmtx_start, pthread_cond_t* tcond_start)
{
	this->size = size;
		this->mtx_start = tmtx_start;
	this->mtx_pause = tmtx_pause;
	this->cond_start = tcond_start;
	this->cond_pause = tcond_pause;

	food = new bool*[size.x];

	for (int i = 0; i < size.x; i++)
	{
		food[i] = new bool[size.y];
	}
	for (int i = 0; i < size.x; i++)
	{
		for (int j = 0; j < size.y; j++)
		{
			food[i][j] = false;
		}
	}

	getch3v2 = std::thread(&Arena::getch3, this);	
	spawner = std::thread(&Arena::spawnFood, this);
}

void Arena::print()
{
	for (int i = 0; i < size.x; i++)
	{
		for (int j = 0; j < size.y; j++)
		{
			if (food[i][j] == true)
			{
				move(i,j);
				attron(COLOR_PAIR(3));
					printw(" ");
				attroff(COLOR_PAIR(3));
				
			}
	
		}
	}

}

Vector2 Arena::findFood(Vector2 position)
{

	float distance = 100000;
	Vector2 result = Vector2(-1,-1);
	//cout << "Find : ";
	for (int i = 0; i < size.x; i++)
	{
		for (int j = 0; j < size.y; j++)
		{
			//cout << i << " " << j << endl;
			if(food[i][j] == true)
			{
				float curDistance = sqrt(pow((position.x - i),2) + pow((position.y - j),2));
				if (curDistance < distance)
				{
									//cout << " X !" << endl;

					distance = curDistance;
					result = Vector2(i, j);
				}	
			}
			
		}
	}

	//cout << result.x << " " << result.y << endl;
	return result;
}

bool Arena::eat(Vector2 position)
{

	if (food[position.x][position.y] == true)
	{
		food[position.x][position.y] = false;
		cout << " zjadlem ";
		return true;
	}
	return false;
}

Vector2 Arena::getSize()
{
	return size;
}

void Arena::spawnFood()
{
	while(true)
	{
		Vector2 tmp;
		tmp.x = std::rand() % size.x;
		tmp.y = std::rand() % size.y;

		//cout <<tmp.x <<" "<< tmp.y<<endl;
		food[tmp.x][tmp.y] = true;

		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}
	
	void Arena::getch3(void)
{
		char key = 1;
	while (key != 'e')
	{
	cin >> key;
	cout << key << endl;
	if(key == 's')
		{
			pthread_cond_broadcast(cond_start);
			ungetch(key);
		}
		if(key == 'p')
		{
			pthread_cond_broadcast(cond_pause);
			ungetch(key);
		}
	}
}


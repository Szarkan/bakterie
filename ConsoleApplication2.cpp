#include "Bakteria1.h"
#include <thread>
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <vector>
#include "randomGenerator.h"
#include <ncurses.h>

using namespace std;

list<Bakteria1*>* List;
Arena *arena;


pthread_mutex_t mtx_pause = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_pause = PTHREAD_COND_INITIALIZER;

pthread_mutex_t mtx_start = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_start = PTHREAD_COND_INITIALIZER;


void print()
{

	initscr();
	start_color();
	init_pair(2,COLOR_BLUE,COLOR_BLACK);
	init_pair(1,COLOR_RED,COLOR_BLACK);
	init_pair(3,COLOR_WHITE, COLOR_GREEN);
	curs_set(0);
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		clear();
		arena->print();
		for (list<Bakteria1*>::iterator i = List->begin(); i != List->end(); ++i)
		{
			//std::cout << (*i)->getPosition().x << " " << (*i)->getPosition().y <<"\n";
				move((*i)->getPosition().x,(*i)->getPosition().y);
				if ((*i)->getType()==1)
				{
					attron(COLOR_PAIR(1));
					printw("X");
					attroff(COLOR_PAIR(1));
				}
				else if ((*i)->getType()==0)
				{
					attron(COLOR_PAIR(2));
					printw("O");
					attroff(COLOR_PAIR(2));
				}

		}


	
	refresh();
	}
	endwin(); 
}
void getch3(void)
{
		char key;
	do
	{

	key = getch();
	cout << key << endl;
	if(key == 's')
		{
			cout << "ssss";
			pthread_cond_broadcast(&cond_start);
			ungetch(key);
		}
		if(key == 'p')
		{
			pthread_cond_broadcast(&cond_pause);
			ungetch(key);
		}
	} while (key != 'e');
}
int getch2(void)
{
	char key = 1;
	do 
	{
				std::cin >> key;
				//cout << key;
		if(key == 's')
		{
			pthread_cond_broadcast(&cond_start);
			ungetch(key);
		}
		if(key == 'p')
		{
			pthread_cond_broadcast(&cond_pause);
			ungetch(key);
		}
cout << key << endl;

	}while (key!='e');

}

int main()
{
	srand(time(NULL));
	
	List = new list<Bakteria1*>;
	arena = new Arena(Vector2(10, 10),&mtx_pause,&cond_pause,&mtx_start,&cond_start);
	randomGenerator * R = new randomGenerator();

	std::thread T = thread(print);
	//std::thread Key = thread(getch3);
	Bakteria1* X ;

	X = new Bakteria1(List, arena,R, 0,&mtx_pause,&cond_pause,&mtx_start,&cond_start);


std::this_thread::sleep_for(std::chrono::milliseconds(3000));

	X = new Bakteria1(List, arena,R, 0,&mtx_pause,&cond_pause,&mtx_start,&cond_start);
std::this_thread::sleep_for(std::chrono::milliseconds(3000));

	X = new Bakteria1(List, arena,R, 0,&mtx_pause,&cond_pause,&mtx_start,&cond_start);

std::this_thread::sleep_for(std::chrono::milliseconds(8000));
	X = new Bakteria1(List, arena,R, 0,&mtx_pause,&cond_pause,&mtx_start,&cond_start);
std::this_thread::sleep_for(std::chrono::milliseconds(3000));

while(1)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			pthread_cond_broadcast(&cond_start);
	std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			pthread_cond_broadcast(&cond_pause);

}

std::this_thread::sleep_for(std::chrono::milliseconds(300000));
	//X = new Bakteria1(List, arena,R, 1,&mtx_pause,&cond_pause,&mtx_start,&cond_start);
		
 	//X = new Bakteria1(List, arena,R, 1,&mtx_pause,&cond_pause,&mtx_start,&cond_start);

//pthread_cond_broadcast(&cond_start);

//pthread_cond_broadcast(&cond_pause);



exit(EXIT_SUCCESS);
}


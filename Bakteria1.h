#pragma once
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <chrono>
#include <list>
#include "Vector2.h"
#include "Arena.h"
#include "randomGenerator.h"
#include <cmath>
using namespace std;



class Bakteria1
{
	int state;
	int hungry;
	bool death;
	bool pause;
	list<Bakteria1*>* bactery_list;
	short type;
	Arena* arena;
	randomGenerator * RandomGenerator;

	thread thread_main;
	thread thread_wait;

	Vector2 position;
	Vector2 arenaSize;


	pthread_mutex_t* mtx_pause; 
	pthread_cond_t* cond_pause;

	pthread_mutex_t* mtx_start; 
	pthread_cond_t* cond_start;

public:




	Bakteria1();
	~Bakteria1();
	Bakteria1(list<Bakteria1*>* lista, Arena * arena, randomGenerator * R, short type, pthread_mutex_t* tmtx_pause, pthread_cond_t* tcond_pause,pthread_mutex_t* tmtx_start, pthread_cond_t* tcond_start);

   	void wait_for_pause();
	void run();
	void runType1();
	void runType0();
	Bakteria1* find_nearest_bactery();
	void move1(Vector2 vector);
	void eat(Bakteria1* food);
	void eat(Vector2 position);

	Bakteria1 check(Vector2 position);
	void wait_for_signal();
	short getType();

	Vector2 getPosition();
	void kill();

	void paint();
};

